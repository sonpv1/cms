package com.cms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cms.domain.DistributionYear;

import com.cms.repository.DistributionYearRepository;
import com.cms.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DistributionYear.
 */
@RestController
@RequestMapping("/api")
public class DistributionYearResource {

    private final Logger log = LoggerFactory.getLogger(DistributionYearResource.class);

    private static final String ENTITY_NAME = "distributionYear";

    private final DistributionYearRepository distributionYearRepository;

    public DistributionYearResource(DistributionYearRepository distributionYearRepository) {
        this.distributionYearRepository = distributionYearRepository;
    }

    /**
     * POST  /distribution-years : Create a new distributionYear.
     *
     * @param distributionYear the distributionYear to create
     * @return the ResponseEntity with status 201 (Created) and with body the new distributionYear, or with status 400 (Bad Request) if the distributionYear has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/distribution-years")
    @Timed
    public ResponseEntity<DistributionYear> createDistributionYear(@RequestBody DistributionYear distributionYear) throws URISyntaxException {
        log.debug("REST request to save DistributionYear : {}", distributionYear);
        if (distributionYear.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new distributionYear cannot already have an ID")).body(null);
        }
        DistributionYear result = distributionYearRepository.save(distributionYear);
        return ResponseEntity.created(new URI("/api/distribution-years/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /distribution-years : Updates an existing distributionYear.
     *
     * @param distributionYear the distributionYear to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated distributionYear,
     * or with status 400 (Bad Request) if the distributionYear is not valid,
     * or with status 500 (Internal Server Error) if the distributionYear couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/distribution-years")
    @Timed
    public ResponseEntity<DistributionYear> updateDistributionYear(@RequestBody DistributionYear distributionYear) throws URISyntaxException {
        log.debug("REST request to update DistributionYear : {}", distributionYear);
        if (distributionYear.getId() == null) {
            return createDistributionYear(distributionYear);
        }
        DistributionYear result = distributionYearRepository.save(distributionYear);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, distributionYear.getId().toString()))
            .body(result);
    }

    /**
     * GET  /distribution-years : get all the distributionYears.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of distributionYears in body
     */
    @GetMapping("/distribution-years")
    @Timed
    public List<DistributionYear> getAllDistributionYears() {
        log.debug("REST request to get all DistributionYears");
        return distributionYearRepository.findAll();
    }

    /**
     * GET  /distribution-years/:id : get the "id" distributionYear.
     *
     * @param id the id of the distributionYear to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the distributionYear, or with status 404 (Not Found)
     */
    @GetMapping("/distribution-years/{id}")
    @Timed
    public ResponseEntity<DistributionYear> getDistributionYear(@PathVariable Long id) {
        log.debug("REST request to get DistributionYear : {}", id);
        DistributionYear distributionYear = distributionYearRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(distributionYear));
    }

    /**
     * DELETE  /distribution-years/:id : delete the "id" distributionYear.
     *
     * @param id the id of the distributionYear to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/distribution-years/{id}")
    @Timed
    public ResponseEntity<Void> deleteDistributionYear(@PathVariable Long id) {
        log.debug("REST request to delete DistributionYear : {}", id);
        distributionYearRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
