package com.cms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cms.domain.Target;

import com.cms.repository.TargetRepository;
import com.cms.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Target.
 */
@RestController
@RequestMapping("/api")
public class TargetResource {

    private final Logger log = LoggerFactory.getLogger(TargetResource.class);

    private static final String ENTITY_NAME = "target";

    private final TargetRepository targetRepository;

    public TargetResource(TargetRepository targetRepository) {
        this.targetRepository = targetRepository;
    }

    /**
     * POST  /targets : Create a new target.
     *
     * @param target the target to create
     * @return the ResponseEntity with status 201 (Created) and with body the new target, or with status 400 (Bad Request) if the target has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/targets")
    @Timed
    public ResponseEntity<Target> createTarget(@RequestBody Target target) throws URISyntaxException {
        log.debug("REST request to save Target : {}", target);
        if (target.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new target cannot already have an ID")).body(null);
        }
        Target result = targetRepository.save(target);
        return ResponseEntity.created(new URI("/api/targets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /targets : Updates an existing target.
     *
     * @param target the target to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated target,
     * or with status 400 (Bad Request) if the target is not valid,
     * or with status 500 (Internal Server Error) if the target couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/targets")
    @Timed
    public ResponseEntity<Target> updateTarget(@RequestBody Target target) throws URISyntaxException {
        log.debug("REST request to update Target : {}", target);
        if (target.getId() == null) {
            return createTarget(target);
        }
        Target result = targetRepository.save(target);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, target.getId().toString()))
            .body(result);
    }

    /**
     * GET  /targets : get all the targets.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of targets in body
     */
    @GetMapping("/targets")
    @Timed
    public List<Target> getAllTargets() {
        log.debug("REST request to get all Targets");
        return targetRepository.findAll();
    }

    /**
     * GET  /targets/:id : get the "id" target.
     *
     * @param id the id of the target to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the target, or with status 404 (Not Found)
     */
    @GetMapping("/targets/{id}")
    @Timed
    public ResponseEntity<Target> getTarget(@PathVariable Long id) {
        log.debug("REST request to get Target : {}", id);
        Target target = targetRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(target));
    }

    /**
     * DELETE  /targets/:id : delete the "id" target.
     *
     * @param id the id of the target to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/targets/{id}")
    @Timed
    public ResponseEntity<Void> deleteTarget(@PathVariable Long id) {
        log.debug("REST request to delete Target : {}", id);
        targetRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
