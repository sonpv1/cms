package com.cms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cms.domain.CmsLog;

import com.cms.repository.CmsLogRepository;
import com.cms.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CmsLog.
 */
@RestController
@RequestMapping("/api")
public class CmsLogResource {

    private final Logger log = LoggerFactory.getLogger(CmsLogResource.class);

    private static final String ENTITY_NAME = "cmsLog";

    private final CmsLogRepository cmsLogRepository;

    public CmsLogResource(CmsLogRepository cmsLogRepository) {
        this.cmsLogRepository = cmsLogRepository;
    }

    /**
     * POST  /cms-logs : Create a new cmsLog.
     *
     * @param cmsLog the cmsLog to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cmsLog, or with status 400 (Bad Request) if the cmsLog has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cms-logs")
    @Timed
    public ResponseEntity<CmsLog> createCmsLog(@RequestBody CmsLog cmsLog) throws URISyntaxException {
        log.debug("REST request to save CmsLog : {}", cmsLog);
        if (cmsLog.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new cmsLog cannot already have an ID")).body(null);
        }
        CmsLog result = cmsLogRepository.save(cmsLog);
        return ResponseEntity.created(new URI("/api/cms-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cms-logs : Updates an existing cmsLog.
     *
     * @param cmsLog the cmsLog to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cmsLog,
     * or with status 400 (Bad Request) if the cmsLog is not valid,
     * or with status 500 (Internal Server Error) if the cmsLog couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cms-logs")
    @Timed
    public ResponseEntity<CmsLog> updateCmsLog(@RequestBody CmsLog cmsLog) throws URISyntaxException {
        log.debug("REST request to update CmsLog : {}", cmsLog);
        if (cmsLog.getId() == null) {
            return createCmsLog(cmsLog);
        }
        CmsLog result = cmsLogRepository.save(cmsLog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cmsLog.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cms-logs : get all the cmsLogs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cmsLogs in body
     */
    @GetMapping("/cms-logs")
    @Timed
    public List<CmsLog> getAllCmsLogs() {
        log.debug("REST request to get all CmsLogs");
        return cmsLogRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /cms-logs/:id : get the "id" cmsLog.
     *
     * @param id the id of the cmsLog to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cmsLog, or with status 404 (Not Found)
     */
    @GetMapping("/cms-logs/{id}")
    @Timed
    public ResponseEntity<CmsLog> getCmsLog(@PathVariable Long id) {
        log.debug("REST request to get CmsLog : {}", id);
        CmsLog cmsLog = cmsLogRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cmsLog));
    }

    /**
     * DELETE  /cms-logs/:id : delete the "id" cmsLog.
     *
     * @param id the id of the cmsLog to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cms-logs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCmsLog(@PathVariable Long id) {
        log.debug("REST request to delete CmsLog : {}", id);
        cmsLogRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
