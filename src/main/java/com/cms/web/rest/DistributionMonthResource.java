package com.cms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cms.domain.DistributionMonth;

import com.cms.repository.DistributionMonthRepository;
import com.cms.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DistributionMonth.
 */
@RestController
@RequestMapping("/api")
public class DistributionMonthResource {

    private final Logger log = LoggerFactory.getLogger(DistributionMonthResource.class);

    private static final String ENTITY_NAME = "distributionMonth";

    private final DistributionMonthRepository distributionMonthRepository;

    public DistributionMonthResource(DistributionMonthRepository distributionMonthRepository) {
        this.distributionMonthRepository = distributionMonthRepository;
    }

    /**
     * POST  /distribution-months : Create a new distributionMonth.
     *
     * @param distributionMonth the distributionMonth to create
     * @return the ResponseEntity with status 201 (Created) and with body the new distributionMonth, or with status 400 (Bad Request) if the distributionMonth has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/distribution-months")
    @Timed
    public ResponseEntity<DistributionMonth> createDistributionMonth(@RequestBody DistributionMonth distributionMonth) throws URISyntaxException {
        log.debug("REST request to save DistributionMonth : {}", distributionMonth);
        if (distributionMonth.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new distributionMonth cannot already have an ID")).body(null);
        }
        DistributionMonth result = distributionMonthRepository.save(distributionMonth);
        return ResponseEntity.created(new URI("/api/distribution-months/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /distribution-months : Updates an existing distributionMonth.
     *
     * @param distributionMonth the distributionMonth to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated distributionMonth,
     * or with status 400 (Bad Request) if the distributionMonth is not valid,
     * or with status 500 (Internal Server Error) if the distributionMonth couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/distribution-months")
    @Timed
    public ResponseEntity<DistributionMonth> updateDistributionMonth(@RequestBody DistributionMonth distributionMonth) throws URISyntaxException {
        log.debug("REST request to update DistributionMonth : {}", distributionMonth);
        if (distributionMonth.getId() == null) {
            return createDistributionMonth(distributionMonth);
        }
        DistributionMonth result = distributionMonthRepository.save(distributionMonth);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, distributionMonth.getId().toString()))
            .body(result);
    }

    /**
     * GET  /distribution-months : get all the distributionMonths.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of distributionMonths in body
     */
    @GetMapping("/distribution-months")
    @Timed
    public List<DistributionMonth> getAllDistributionMonths() {
        log.debug("REST request to get all DistributionMonths");
        return distributionMonthRepository.findAll();
    }

    /**
     * GET  /distribution-months/:id : get the "id" distributionMonth.
     *
     * @param id the id of the distributionMonth to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the distributionMonth, or with status 404 (Not Found)
     */
    @GetMapping("/distribution-months/{id}")
    @Timed
    public ResponseEntity<DistributionMonth> getDistributionMonth(@PathVariable Long id) {
        log.debug("REST request to get DistributionMonth : {}", id);
        DistributionMonth distributionMonth = distributionMonthRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(distributionMonth));
    }

    /**
     * DELETE  /distribution-months/:id : delete the "id" distributionMonth.
     *
     * @param id the id of the distributionMonth to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/distribution-months/{id}")
    @Timed
    public ResponseEntity<Void> deleteDistributionMonth(@PathVariable Long id) {
        log.debug("REST request to delete DistributionMonth : {}", id);
        distributionMonthRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
