package com.cms.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cms.domain.TargetMetric;

import com.cms.repository.TargetMetricRepository;
import com.cms.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TargetMetric.
 */
@RestController
@RequestMapping("/api")
public class TargetMetricResource {

    private final Logger log = LoggerFactory.getLogger(TargetMetricResource.class);

    private static final String ENTITY_NAME = "targetMetric";

    private final TargetMetricRepository targetMetricRepository;

    public TargetMetricResource(TargetMetricRepository targetMetricRepository) {
        this.targetMetricRepository = targetMetricRepository;
    }

    /**
     * POST  /target-metrics : Create a new targetMetric.
     *
     * @param targetMetric the targetMetric to create
     * @return the ResponseEntity with status 201 (Created) and with body the new targetMetric, or with status 400 (Bad Request) if the targetMetric has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/target-metrics")
    @Timed
    public ResponseEntity<TargetMetric> createTargetMetric(@RequestBody TargetMetric targetMetric) throws URISyntaxException {
        log.debug("REST request to save TargetMetric : {}", targetMetric);
        if (targetMetric.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new targetMetric cannot already have an ID")).body(null);
        }
        TargetMetric result = targetMetricRepository.save(targetMetric);
        return ResponseEntity.created(new URI("/api/target-metrics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /target-metrics : Updates an existing targetMetric.
     *
     * @param targetMetric the targetMetric to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated targetMetric,
     * or with status 400 (Bad Request) if the targetMetric is not valid,
     * or with status 500 (Internal Server Error) if the targetMetric couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/target-metrics")
    @Timed
    public ResponseEntity<TargetMetric> updateTargetMetric(@RequestBody TargetMetric targetMetric) throws URISyntaxException {
        log.debug("REST request to update TargetMetric : {}", targetMetric);
        if (targetMetric.getId() == null) {
            return createTargetMetric(targetMetric);
        }
        TargetMetric result = targetMetricRepository.save(targetMetric);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, targetMetric.getId().toString()))
            .body(result);
    }

    /**
     * GET  /target-metrics : get all the targetMetrics.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of targetMetrics in body
     */
    @GetMapping("/target-metrics")
    @Timed
    public List<TargetMetric> getAllTargetMetrics() {
        log.debug("REST request to get all TargetMetrics");
        return targetMetricRepository.findAll();
    }

    /**
     * GET  /target-metrics/:id : get the "id" targetMetric.
     *
     * @param id the id of the targetMetric to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the targetMetric, or with status 404 (Not Found)
     */
    @GetMapping("/target-metrics/{id}")
    @Timed
    public ResponseEntity<TargetMetric> getTargetMetric(@PathVariable Long id) {
        log.debug("REST request to get TargetMetric : {}", id);
        TargetMetric targetMetric = targetMetricRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(targetMetric));
    }

    /**
     * DELETE  /target-metrics/:id : delete the "id" targetMetric.
     *
     * @param id the id of the targetMetric to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/target-metrics/{id}")
    @Timed
    public ResponseEntity<Void> deleteTargetMetric(@PathVariable Long id) {
        log.debug("REST request to delete TargetMetric : {}", id);
        targetMetricRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
