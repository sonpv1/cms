package com.cms.repository;

import com.cms.domain.TargetMetric;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TargetMetric entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TargetMetricRepository extends JpaRepository<TargetMetric,Long> {
    
}
