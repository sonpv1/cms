package com.cms.repository;

import com.cms.domain.DistributionYear;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DistributionYear entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistributionYearRepository extends JpaRepository<DistributionYear,Long> {
    
}
