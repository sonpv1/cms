package com.cms.repository;

import com.cms.domain.DistributionMonth;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DistributionMonth entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistributionMonthRepository extends JpaRepository<DistributionMonth,Long> {
    
}
