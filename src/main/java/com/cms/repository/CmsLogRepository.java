package com.cms.repository;

import com.cms.domain.CmsLog;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the CmsLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CmsLogRepository extends JpaRepository<CmsLog,Long> {
    
    @Query("select distinct cms_log from CmsLog cms_log left join fetch cms_log.users")
    List<CmsLog> findAllWithEagerRelationships();

    @Query("select cms_log from CmsLog cms_log left join fetch cms_log.users where cms_log.id =:id")
    CmsLog findOneWithEagerRelationships(@Param("id") Long id);
    
}
