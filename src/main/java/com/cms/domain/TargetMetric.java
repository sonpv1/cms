package com.cms.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TargetMetric.
 */
@Entity
@Table(name = "target_metric")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TargetMetric implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "department")
    private String department;

    @Column(name = "metric")
    private Integer metric;

    @Column(name = "cms_cost")
    private Integer cost;

    @Column(name = "cost_type")
    private String costType;

    @Column(name = "last_update")
    private Long lastUpdate;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    private Group group;

    @ManyToOne
    private Target target;

    @ManyToOne
    private DistributionMonth distributionMonth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public TargetMetric name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public TargetMetric department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getMetric() {
        return metric;
    }

    public TargetMetric metric(Integer metric) {
        this.metric = metric;
        return this;
    }

    public void setMetric(Integer metric) {
        this.metric = metric;
    }

    public Integer getCost() {
        return cost;
    }

    public TargetMetric cost(Integer cost) {
        this.cost = cost;
        return this;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getCostType() {
        return costType;
    }

    public TargetMetric costType(String costType) {
        this.costType = costType;
        return this;
    }

    public void setCostType(String costType) {
        this.costType = costType;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public TargetMetric lastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isStatus() {
        return status;
    }

    public TargetMetric status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Group getGroup() {
        return group;
    }

    public TargetMetric group(Group group) {
        this.group = group;
        return this;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Target getTarget() {
        return target;
    }

    public TargetMetric target(Target target) {
        this.target = target;
        return this;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public DistributionMonth getDistributionMonth() {
        return distributionMonth;
    }

    public TargetMetric distributionMonth(DistributionMonth distributionMonth) {
        this.distributionMonth = distributionMonth;
        return this;
    }

    public void setDistributionMonth(DistributionMonth distributionMonth) {
        this.distributionMonth = distributionMonth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TargetMetric targetMetric = (TargetMetric) o;
        if (targetMetric.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), targetMetric.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TargetMetric{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", department='" + getDepartment() + "'" +
            ", metric='" + getMetric() + "'" +
            ", cost='" + getCost() + "'" +
            ", costType='" + getCostType() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
