package com.cms.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Target.
 */
@Entity
@Table(name = "target")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Target implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "last_update")
    private Long lastUpdate;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy = "target")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DistributionYear> years = new HashSet<>();

    @OneToMany(mappedBy = "target")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TargetMetric> metrics = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Target name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public Target lastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isStatus() {
        return status;
    }

    public Target status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Set<DistributionYear> getYears() {
        return years;
    }

    public Target years(Set<DistributionYear> distributionYears) {
        this.years = distributionYears;
        return this;
    }

    public Target addYear(DistributionYear distributionYear) {
        this.years.add(distributionYear);
        distributionYear.setTarget(this);
        return this;
    }

    public Target removeYear(DistributionYear distributionYear) {
        this.years.remove(distributionYear);
        distributionYear.setTarget(null);
        return this;
    }

    public void setYears(Set<DistributionYear> distributionYears) {
        this.years = distributionYears;
    }

    public Set<TargetMetric> getMetrics() {
        return metrics;
    }

    public Target metrics(Set<TargetMetric> targetMetrics) {
        this.metrics = targetMetrics;
        return this;
    }

    public Target addMetric(TargetMetric targetMetric) {
        this.metrics.add(targetMetric);
        targetMetric.setTarget(this);
        return this;
    }

    public Target removeMetric(TargetMetric targetMetric) {
        this.metrics.remove(targetMetric);
        targetMetric.setTarget(null);
        return this;
    }

    public void setMetrics(Set<TargetMetric> targetMetrics) {
        this.metrics = targetMetrics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Target target = (Target) o;
        if (target.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), target.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Target{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
