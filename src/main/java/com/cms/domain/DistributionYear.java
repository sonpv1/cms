package com.cms.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DistributionYear.
 */
@Entity
@Table(name = "distribution_year")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DistributionYear implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "last_update")
    private Long lastUpdate;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    private Target target;

    @OneToMany(mappedBy = "distributionYear")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DistributionMonth> months = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public DistributionYear name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public DistributionYear lastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isStatus() {
        return status;
    }

    public DistributionYear status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Target getTarget() {
        return target;
    }

    public DistributionYear target(Target target) {
        this.target = target;
        return this;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public Set<DistributionMonth> getMonths() {
        return months;
    }

    public DistributionYear months(Set<DistributionMonth> distributionMonths) {
        this.months = distributionMonths;
        return this;
    }

    public DistributionYear addMonth(DistributionMonth distributionMonth) {
        this.months.add(distributionMonth);
        distributionMonth.setDistributionYear(this);
        return this;
    }

    public DistributionYear removeMonth(DistributionMonth distributionMonth) {
        this.months.remove(distributionMonth);
        distributionMonth.setDistributionYear(null);
        return this;
    }

    public void setMonths(Set<DistributionMonth> distributionMonths) {
        this.months = distributionMonths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DistributionYear distributionYear = (DistributionYear) o;
        if (distributionYear.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), distributionYear.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DistributionYear{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
