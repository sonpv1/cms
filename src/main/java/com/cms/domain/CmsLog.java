package com.cms.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CmsLog.
 */
@Entity
@Table(name = "cms_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CmsLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "action")
    private String action;

    @Column(name = "last_update")
    private Long lastUpdate;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "cms_log_user",
               joinColumns = @JoinColumn(name="cms_logs_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="users_id", referencedColumnName="id"))
    private Set<User> users = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public CmsLog action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public CmsLog lastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<User> getUsers() {
        return users;
    }

    public CmsLog users(Set<User> users) {
        this.users = users;
        return this;
    }

    public CmsLog addUser(User user) {
        this.users.add(user);
        user.getLogs().add(this);
        return this;
    }

    public CmsLog removeUser(User user) {
        this.users.remove(user);
        user.getLogs().remove(this);
        return this;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CmsLog cmsLog = (CmsLog) o;
        if (cmsLog.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cmsLog.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CmsLog{" +
            "id=" + getId() +
            ", action='" + getAction() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
