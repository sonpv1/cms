package com.cms.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DistributionMonth.
 */
@Entity
@Table(name = "distribution_month")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DistributionMonth implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "last_update")
    private Long lastUpdate;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    private DistributionYear distributionYear;

    @OneToMany(mappedBy = "distributionMonth")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TargetMetric> months = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public DistributionMonth name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public DistributionMonth lastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean isStatus() {
        return status;
    }

    public DistributionMonth status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public DistributionYear getDistributionYear() {
        return distributionYear;
    }

    public DistributionMonth distributionYear(DistributionYear distributionYear) {
        this.distributionYear = distributionYear;
        return this;
    }

    public void setDistributionYear(DistributionYear distributionYear) {
        this.distributionYear = distributionYear;
    }

    public Set<TargetMetric> getMonths() {
        return months;
    }

    public DistributionMonth months(Set<TargetMetric> targetMetrics) {
        this.months = targetMetrics;
        return this;
    }

    public DistributionMonth addMonth(TargetMetric targetMetric) {
        this.months.add(targetMetric);
        targetMetric.setDistributionMonth(this);
        return this;
    }

    public DistributionMonth removeMonth(TargetMetric targetMetric) {
        this.months.remove(targetMetric);
        targetMetric.setDistributionMonth(null);
        return this;
    }

    public void setMonths(Set<TargetMetric> targetMetrics) {
        this.months = targetMetrics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DistributionMonth distributionMonth = (DistributionMonth) o;
        if (distributionMonth.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), distributionMonth.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DistributionMonth{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
