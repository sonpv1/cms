package com.cms.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.cms.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.User.class.getName() + ".logs", jcacheConfiguration);
            cm.createCache(com.cms.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.cms.domain.PersistentToken.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.User.class.getName() + ".persistentTokens", jcacheConfiguration);
            cm.createCache(com.cms.domain.Group.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.Group.class.getName() + ".users", jcacheConfiguration);
            cm.createCache(com.cms.domain.Group.class.getName() + ".departments", jcacheConfiguration);
            cm.createCache(com.cms.domain.Partner.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.Partner.class.getName() + ".users", jcacheConfiguration);
            cm.createCache(com.cms.domain.TargetMetric.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.Target.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.Target.class.getName() + ".years", jcacheConfiguration);
            cm.createCache(com.cms.domain.Target.class.getName() + ".metrics", jcacheConfiguration);
            cm.createCache(com.cms.domain.CmsLog.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.CmsLog.class.getName() + ".users", jcacheConfiguration);
            cm.createCache(com.cms.domain.DistributionYear.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.DistributionYear.class.getName() + ".months", jcacheConfiguration);
            cm.createCache(com.cms.domain.DistributionMonth.class.getName(), jcacheConfiguration);
            cm.createCache(com.cms.domain.DistributionMonth.class.getName() + ".months", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
