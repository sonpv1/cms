(function() {
    'use strict';

    angular
        .module('cmsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('distribution-year', {
            parent: 'entity',
            url: '/distribution-year',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.distributionYear.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribution-year/distribution-years.html',
                    controller: 'DistributionYearController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('distributionYear');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('distribution-year-detail', {
            parent: 'distribution-year',
            url: '/distribution-year/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.distributionYear.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribution-year/distribution-year-detail.html',
                    controller: 'DistributionYearDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('distributionYear');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DistributionYear', function($stateParams, DistributionYear) {
                    return DistributionYear.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'distribution-year',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('distribution-year-detail.edit', {
            parent: 'distribution-year-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-year/distribution-year-dialog.html',
                    controller: 'DistributionYearDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DistributionYear', function(DistributionYear) {
                            return DistributionYear.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('distribution-year.new', {
            parent: 'distribution-year',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-year/distribution-year-dialog.html',
                    controller: 'DistributionYearDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                lastUpdate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('distribution-year', null, { reload: 'distribution-year' });
                }, function() {
                    $state.go('distribution-year');
                });
            }]
        })
        .state('distribution-year.edit', {
            parent: 'distribution-year',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-year/distribution-year-dialog.html',
                    controller: 'DistributionYearDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DistributionYear', function(DistributionYear) {
                            return DistributionYear.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('distribution-year', null, { reload: 'distribution-year' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('distribution-year.delete', {
            parent: 'distribution-year',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-year/distribution-year-delete-dialog.html',
                    controller: 'DistributionYearDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DistributionYear', function(DistributionYear) {
                            return DistributionYear.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('distribution-year', null, { reload: 'distribution-year' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
