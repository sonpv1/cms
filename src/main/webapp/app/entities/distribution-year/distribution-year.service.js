(function() {
    'use strict';
    angular
        .module('cmsApp')
        .factory('DistributionYear', DistributionYear);

    DistributionYear.$inject = ['$resource'];

    function DistributionYear ($resource) {
        var resourceUrl =  'api/distribution-years/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
