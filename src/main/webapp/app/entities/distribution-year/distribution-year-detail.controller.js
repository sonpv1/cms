(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionYearDetailController', DistributionYearDetailController);

    DistributionYearDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DistributionYear', 'Target', 'DistributionMonth'];

    function DistributionYearDetailController($scope, $rootScope, $stateParams, previousState, entity, DistributionYear, Target, DistributionMonth) {
        var vm = this;

        vm.distributionYear = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:distributionYearUpdate', function(event, result) {
            vm.distributionYear = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
