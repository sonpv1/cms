(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionYearController', DistributionYearController);

    DistributionYearController.$inject = ['DistributionYear'];

    function DistributionYearController(DistributionYear) {

        var vm = this;

        vm.distributionYears = [];

        loadAll();

        function loadAll() {
            DistributionYear.query(function(result) {
                vm.distributionYears = result;
                vm.searchQuery = null;
            });
        }
    }
})();
