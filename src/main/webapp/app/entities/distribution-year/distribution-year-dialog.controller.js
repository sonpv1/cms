(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionYearDialogController', DistributionYearDialogController);

    DistributionYearDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DistributionYear', 'Target', 'DistributionMonth'];

    function DistributionYearDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DistributionYear, Target, DistributionMonth) {
        var vm = this;

        vm.distributionYear = entity;
        vm.clear = clear;
        vm.save = save;
        vm.targets = Target.query();
        vm.distributionmonths = DistributionMonth.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.distributionYear.id !== null) {
                DistributionYear.update(vm.distributionYear, onSaveSuccess, onSaveError);
            } else {
                DistributionYear.save(vm.distributionYear, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cmsApp:distributionYearUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
