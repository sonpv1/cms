(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionYearDeleteController',DistributionYearDeleteController);

    DistributionYearDeleteController.$inject = ['$uibModalInstance', 'entity', 'DistributionYear'];

    function DistributionYearDeleteController($uibModalInstance, entity, DistributionYear) {
        var vm = this;

        vm.distributionYear = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DistributionYear.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
