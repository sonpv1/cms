(function() {
    'use strict';
    angular
        .module('cmsApp')
        .factory('Target', Target);

    Target.$inject = ['$resource'];

    function Target ($resource) {
        var resourceUrl =  'api/targets/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
