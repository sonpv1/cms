(function() {
    'use strict';

    angular
        .module('cmsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('target', {
            parent: 'entity',
            url: '/target',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.target.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/target/targets.html',
                    controller: 'TargetController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('target');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('target-detail', {
            parent: 'target',
            url: '/target/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.target.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/target/target-detail.html',
                    controller: 'TargetDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('target');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Target', function($stateParams, Target) {
                    return Target.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'target',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('target-detail.edit', {
            parent: 'target-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target/target-dialog.html',
                    controller: 'TargetDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Target', function(Target) {
                            return Target.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('target.new', {
            parent: 'target',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target/target-dialog.html',
                    controller: 'TargetDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                lastUpdate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('target', null, { reload: 'target' });
                }, function() {
                    $state.go('target');
                });
            }]
        })
        .state('target.edit', {
            parent: 'target',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target/target-dialog.html',
                    controller: 'TargetDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Target', function(Target) {
                            return Target.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('target', null, { reload: 'target' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('target.delete', {
            parent: 'target',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target/target-delete-dialog.html',
                    controller: 'TargetDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Target', function(Target) {
                            return Target.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('target', null, { reload: 'target' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
