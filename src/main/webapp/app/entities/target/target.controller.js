(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetController', TargetController);

    TargetController.$inject = ['Target'];

    function TargetController(Target) {

        var vm = this;

        vm.targets = [];

        loadAll();

        function loadAll() {
            Target.query(function(result) {
                vm.targets = result;
                vm.searchQuery = null;
            });
        }
    }
})();
