(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetDetailController', TargetDetailController);

    TargetDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Target', 'DistributionYear', 'TargetMetric'];

    function TargetDetailController($scope, $rootScope, $stateParams, previousState, entity, Target, DistributionYear, TargetMetric) {
        var vm = this;

        vm.target = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:targetUpdate', function(event, result) {
            vm.target = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
