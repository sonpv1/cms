(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetDialogController', TargetDialogController);

    TargetDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Target', 'DistributionYear', 'TargetMetric'];

    function TargetDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Target, DistributionYear, TargetMetric) {
        var vm = this;

        vm.target = entity;
        vm.clear = clear;
        vm.save = save;
        vm.distributionyears = DistributionYear.query();
        vm.targetmetrics = TargetMetric.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.target.id !== null) {
                Target.update(vm.target, onSaveSuccess, onSaveError);
            } else {
                Target.save(vm.target, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cmsApp:targetUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
