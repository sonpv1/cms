(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('PartnerController', PartnerController);

    PartnerController.$inject = ['Partner'];

    function PartnerController(Partner) {

        var vm = this;

        vm.partners = [];

        loadAll();

        function loadAll() {
            Partner.query(function(result) {
                vm.partners = result;
                vm.searchQuery = null;
            });
        }
    }
})();
