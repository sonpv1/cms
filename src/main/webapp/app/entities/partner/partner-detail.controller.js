(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('PartnerDetailController', PartnerDetailController);

    PartnerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Partner', 'User'];

    function PartnerDetailController($scope, $rootScope, $stateParams, previousState, entity, Partner, User) {
        var vm = this;

        vm.partner = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:partnerUpdate', function(event, result) {
            vm.partner = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
