(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetMetricController', TargetMetricController);

    TargetMetricController.$inject = ['TargetMetric'];

    function TargetMetricController(TargetMetric) {

        var vm = this;

        vm.targetMetrics = [];

        loadAll();

        function loadAll() {
            TargetMetric.query(function(result) {
                vm.targetMetrics = result;
                vm.searchQuery = null;
            });
        }
    }
})();
