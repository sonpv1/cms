(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetMetricDialogController', TargetMetricDialogController);

    TargetMetricDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TargetMetric', 'Group', 'Target', 'DistributionMonth'];

    function TargetMetricDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TargetMetric, Group, Target, DistributionMonth) {
        var vm = this;

        vm.targetMetric = entity;
        vm.clear = clear;
        vm.save = save;
        vm.groups = Group.query();
        vm.targets = Target.query();
        vm.distributionmonths = DistributionMonth.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.targetMetric.id !== null) {
                TargetMetric.update(vm.targetMetric, onSaveSuccess, onSaveError);
            } else {
                TargetMetric.save(vm.targetMetric, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cmsApp:targetMetricUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
