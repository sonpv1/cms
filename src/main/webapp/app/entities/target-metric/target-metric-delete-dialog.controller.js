(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetMetricDeleteController',TargetMetricDeleteController);

    TargetMetricDeleteController.$inject = ['$uibModalInstance', 'entity', 'TargetMetric'];

    function TargetMetricDeleteController($uibModalInstance, entity, TargetMetric) {
        var vm = this;

        vm.targetMetric = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TargetMetric.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
