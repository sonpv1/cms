(function() {
    'use strict';
    angular
        .module('cmsApp')
        .factory('TargetMetric', TargetMetric);

    TargetMetric.$inject = ['$resource'];

    function TargetMetric ($resource) {
        var resourceUrl =  'api/target-metrics/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
