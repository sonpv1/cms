(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('TargetMetricDetailController', TargetMetricDetailController);

    TargetMetricDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TargetMetric', 'Group', 'Target', 'DistributionMonth'];

    function TargetMetricDetailController($scope, $rootScope, $stateParams, previousState, entity, TargetMetric, Group, Target, DistributionMonth) {
        var vm = this;

        vm.targetMetric = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:targetMetricUpdate', function(event, result) {
            vm.targetMetric = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
