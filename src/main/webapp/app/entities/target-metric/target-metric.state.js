(function() {
    'use strict';

    angular
        .module('cmsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('target-metric', {
            parent: 'entity',
            url: '/target-metric',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.targetMetric.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/target-metric/target-metrics.html',
                    controller: 'TargetMetricController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('targetMetric');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('target-metric-detail', {
            parent: 'target-metric',
            url: '/target-metric/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.targetMetric.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/target-metric/target-metric-detail.html',
                    controller: 'TargetMetricDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('targetMetric');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TargetMetric', function($stateParams, TargetMetric) {
                    return TargetMetric.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'target-metric',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('target-metric-detail.edit', {
            parent: 'target-metric-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target-metric/target-metric-dialog.html',
                    controller: 'TargetMetricDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TargetMetric', function(TargetMetric) {
                            return TargetMetric.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('target-metric.new', {
            parent: 'target-metric',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target-metric/target-metric-dialog.html',
                    controller: 'TargetMetricDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                department: null,
                                metric: null,
                                cost: null,
                                costType: null,
                                lastUpdate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('target-metric', null, { reload: 'target-metric' });
                }, function() {
                    $state.go('target-metric');
                });
            }]
        })
        .state('target-metric.edit', {
            parent: 'target-metric',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target-metric/target-metric-dialog.html',
                    controller: 'TargetMetricDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TargetMetric', function(TargetMetric) {
                            return TargetMetric.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('target-metric', null, { reload: 'target-metric' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('target-metric.delete', {
            parent: 'target-metric',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/target-metric/target-metric-delete-dialog.html',
                    controller: 'TargetMetricDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TargetMetric', function(TargetMetric) {
                            return TargetMetric.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('target-metric', null, { reload: 'target-metric' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
