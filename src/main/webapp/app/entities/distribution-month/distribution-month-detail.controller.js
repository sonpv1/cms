(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionMonthDetailController', DistributionMonthDetailController);

    DistributionMonthDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DistributionMonth', 'DistributionYear', 'TargetMetric'];

    function DistributionMonthDetailController($scope, $rootScope, $stateParams, previousState, entity, DistributionMonth, DistributionYear, TargetMetric) {
        var vm = this;

        vm.distributionMonth = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:distributionMonthUpdate', function(event, result) {
            vm.distributionMonth = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
