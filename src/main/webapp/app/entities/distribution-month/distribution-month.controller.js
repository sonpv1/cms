(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionMonthController', DistributionMonthController);

    DistributionMonthController.$inject = ['DistributionMonth'];

    function DistributionMonthController(DistributionMonth) {

        var vm = this;

        vm.distributionMonths = [];

        loadAll();

        function loadAll() {
            DistributionMonth.query(function(result) {
                vm.distributionMonths = result;
                vm.searchQuery = null;
            });
        }
    }
})();
