(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionMonthDialogController', DistributionMonthDialogController);

    DistributionMonthDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DistributionMonth', 'DistributionYear', 'TargetMetric'];

    function DistributionMonthDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DistributionMonth, DistributionYear, TargetMetric) {
        var vm = this;

        vm.distributionMonth = entity;
        vm.clear = clear;
        vm.save = save;
        vm.distributionyears = DistributionYear.query();
        vm.targetmetrics = TargetMetric.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.distributionMonth.id !== null) {
                DistributionMonth.update(vm.distributionMonth, onSaveSuccess, onSaveError);
            } else {
                DistributionMonth.save(vm.distributionMonth, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cmsApp:distributionMonthUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
