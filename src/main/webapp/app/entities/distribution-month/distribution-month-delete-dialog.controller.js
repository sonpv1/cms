(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('DistributionMonthDeleteController',DistributionMonthDeleteController);

    DistributionMonthDeleteController.$inject = ['$uibModalInstance', 'entity', 'DistributionMonth'];

    function DistributionMonthDeleteController($uibModalInstance, entity, DistributionMonth) {
        var vm = this;

        vm.distributionMonth = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DistributionMonth.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
