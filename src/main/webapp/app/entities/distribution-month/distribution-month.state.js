(function() {
    'use strict';

    angular
        .module('cmsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('distribution-month', {
            parent: 'entity',
            url: '/distribution-month',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.distributionMonth.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribution-month/distribution-months.html',
                    controller: 'DistributionMonthController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('distributionMonth');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('distribution-month-detail', {
            parent: 'distribution-month',
            url: '/distribution-month/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.distributionMonth.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribution-month/distribution-month-detail.html',
                    controller: 'DistributionMonthDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('distributionMonth');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DistributionMonth', function($stateParams, DistributionMonth) {
                    return DistributionMonth.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'distribution-month',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('distribution-month-detail.edit', {
            parent: 'distribution-month-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-month/distribution-month-dialog.html',
                    controller: 'DistributionMonthDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DistributionMonth', function(DistributionMonth) {
                            return DistributionMonth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('distribution-month.new', {
            parent: 'distribution-month',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-month/distribution-month-dialog.html',
                    controller: 'DistributionMonthDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                lastUpdate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('distribution-month', null, { reload: 'distribution-month' });
                }, function() {
                    $state.go('distribution-month');
                });
            }]
        })
        .state('distribution-month.edit', {
            parent: 'distribution-month',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-month/distribution-month-dialog.html',
                    controller: 'DistributionMonthDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DistributionMonth', function(DistributionMonth) {
                            return DistributionMonth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('distribution-month', null, { reload: 'distribution-month' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('distribution-month.delete', {
            parent: 'distribution-month',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/distribution-month/distribution-month-delete-dialog.html',
                    controller: 'DistributionMonthDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DistributionMonth', function(DistributionMonth) {
                            return DistributionMonth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('distribution-month', null, { reload: 'distribution-month' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
