(function() {
    'use strict';
    angular
        .module('cmsApp')
        .factory('DistributionMonth', DistributionMonth);

    DistributionMonth.$inject = ['$resource'];

    function DistributionMonth ($resource) {
        var resourceUrl =  'api/distribution-months/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
