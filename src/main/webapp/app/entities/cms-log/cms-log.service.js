(function() {
    'use strict';
    angular
        .module('cmsApp')
        .factory('CmsLog', CmsLog);

    CmsLog.$inject = ['$resource'];

    function CmsLog ($resource) {
        var resourceUrl =  'api/cms-logs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
