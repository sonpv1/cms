(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('CmsLogDetailController', CmsLogDetailController);

    CmsLogDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CmsLog', 'User'];

    function CmsLogDetailController($scope, $rootScope, $stateParams, previousState, entity, CmsLog, User) {
        var vm = this;

        vm.cmsLog = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:cmsLogUpdate', function(event, result) {
            vm.cmsLog = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
