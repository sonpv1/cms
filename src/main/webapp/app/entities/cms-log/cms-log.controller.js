(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('CmsLogController', CmsLogController);

    CmsLogController.$inject = ['CmsLog'];

    function CmsLogController(CmsLog) {

        var vm = this;

        vm.cmsLogs = [];

        loadAll();

        function loadAll() {
            CmsLog.query(function(result) {
                vm.cmsLogs = result;
                vm.searchQuery = null;
            });
        }
    }
})();
