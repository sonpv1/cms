(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('CmsLogDialogController', CmsLogDialogController);

    CmsLogDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'CmsLog', 'User'];

    function CmsLogDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, CmsLog, User) {
        var vm = this;

        vm.cmsLog = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.cmsLog.id !== null) {
                CmsLog.update(vm.cmsLog, onSaveSuccess, onSaveError);
            } else {
                CmsLog.save(vm.cmsLog, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cmsApp:cmsLogUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
