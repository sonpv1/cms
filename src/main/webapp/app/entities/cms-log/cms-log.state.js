(function() {
    'use strict';

    angular
        .module('cmsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('cms-log', {
            parent: 'entity',
            url: '/cms-log',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.cmsLog.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cms-log/cms-logs.html',
                    controller: 'CmsLogController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cmsLog');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('cms-log-detail', {
            parent: 'cms-log',
            url: '/cms-log/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cmsApp.cmsLog.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cms-log/cms-log-detail.html',
                    controller: 'CmsLogDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cmsLog');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CmsLog', function($stateParams, CmsLog) {
                    return CmsLog.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'cms-log',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('cms-log-detail.edit', {
            parent: 'cms-log-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cms-log/cms-log-dialog.html',
                    controller: 'CmsLogDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CmsLog', function(CmsLog) {
                            return CmsLog.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('cms-log.new', {
            parent: 'cms-log',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cms-log/cms-log-dialog.html',
                    controller: 'CmsLogDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                action: null,
                                lastUpdate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('cms-log', null, { reload: 'cms-log' });
                }, function() {
                    $state.go('cms-log');
                });
            }]
        })
        .state('cms-log.edit', {
            parent: 'cms-log',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cms-log/cms-log-dialog.html',
                    controller: 'CmsLogDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CmsLog', function(CmsLog) {
                            return CmsLog.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('cms-log', null, { reload: 'cms-log' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('cms-log.delete', {
            parent: 'cms-log',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cms-log/cms-log-delete-dialog.html',
                    controller: 'CmsLogDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CmsLog', function(CmsLog) {
                            return CmsLog.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('cms-log', null, { reload: 'cms-log' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
