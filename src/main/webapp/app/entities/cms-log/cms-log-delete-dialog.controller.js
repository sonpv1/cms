(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('CmsLogDeleteController',CmsLogDeleteController);

    CmsLogDeleteController.$inject = ['$uibModalInstance', 'entity', 'CmsLog'];

    function CmsLogDeleteController($uibModalInstance, entity, CmsLog) {
        var vm = this;

        vm.cmsLog = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CmsLog.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
