(function() {
    'use strict';

    angular
        .module('cmsApp')
        .controller('GroupDetailController', GroupDetailController);

    GroupDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Group', 'User', 'TargetMetric'];

    function GroupDetailController($scope, $rootScope, $stateParams, previousState, entity, Group, User, TargetMetric) {
        var vm = this;

        vm.group = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cmsApp:groupUpdate', function(event, result) {
            vm.group = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
