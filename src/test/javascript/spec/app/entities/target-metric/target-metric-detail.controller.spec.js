'use strict';

describe('Controller Tests', function() {

    describe('TargetMetric Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTargetMetric, MockGroup, MockTarget, MockDistributionMonth;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTargetMetric = jasmine.createSpy('MockTargetMetric');
            MockGroup = jasmine.createSpy('MockGroup');
            MockTarget = jasmine.createSpy('MockTarget');
            MockDistributionMonth = jasmine.createSpy('MockDistributionMonth');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'TargetMetric': MockTargetMetric,
                'Group': MockGroup,
                'Target': MockTarget,
                'DistributionMonth': MockDistributionMonth
            };
            createController = function() {
                $injector.get('$controller')("TargetMetricDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'cmsApp:targetMetricUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
