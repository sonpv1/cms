'use strict';

describe('Controller Tests', function() {

    describe('DistributionMonth Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockDistributionMonth, MockDistributionYear, MockTargetMetric;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockDistributionMonth = jasmine.createSpy('MockDistributionMonth');
            MockDistributionYear = jasmine.createSpy('MockDistributionYear');
            MockTargetMetric = jasmine.createSpy('MockTargetMetric');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'DistributionMonth': MockDistributionMonth,
                'DistributionYear': MockDistributionYear,
                'TargetMetric': MockTargetMetric
            };
            createController = function() {
                $injector.get('$controller')("DistributionMonthDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'cmsApp:distributionMonthUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
