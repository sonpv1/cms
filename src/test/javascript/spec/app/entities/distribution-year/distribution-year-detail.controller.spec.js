'use strict';

describe('Controller Tests', function() {

    describe('DistributionYear Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockDistributionYear, MockTarget, MockDistributionMonth;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockDistributionYear = jasmine.createSpy('MockDistributionYear');
            MockTarget = jasmine.createSpy('MockTarget');
            MockDistributionMonth = jasmine.createSpy('MockDistributionMonth');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'DistributionYear': MockDistributionYear,
                'Target': MockTarget,
                'DistributionMonth': MockDistributionMonth
            };
            createController = function() {
                $injector.get('$controller')("DistributionYearDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'cmsApp:distributionYearUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
