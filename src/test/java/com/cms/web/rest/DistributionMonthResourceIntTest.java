package com.cms.web.rest;

import com.cms.CmsApp;

import com.cms.domain.DistributionMonth;
import com.cms.repository.DistributionMonthRepository;
import com.cms.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DistributionMonthResource REST controller.
 *
 * @see DistributionMonthResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CmsApp.class)
public class DistributionMonthResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_LAST_UPDATE = 1L;
    private static final Long UPDATED_LAST_UPDATE = 2L;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private DistributionMonthRepository distributionMonthRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDistributionMonthMockMvc;

    private DistributionMonth distributionMonth;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DistributionMonthResource distributionMonthResource = new DistributionMonthResource(distributionMonthRepository);
        this.restDistributionMonthMockMvc = MockMvcBuilders.standaloneSetup(distributionMonthResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DistributionMonth createEntity(EntityManager em) {
        DistributionMonth distributionMonth = new DistributionMonth()
            .name(DEFAULT_NAME)
            .lastUpdate(DEFAULT_LAST_UPDATE)
            .status(DEFAULT_STATUS);
        return distributionMonth;
    }

    @Before
    public void initTest() {
        distributionMonth = createEntity(em);
    }

    @Test
    @Transactional
    public void createDistributionMonth() throws Exception {
        int databaseSizeBeforeCreate = distributionMonthRepository.findAll().size();

        // Create the DistributionMonth
        restDistributionMonthMockMvc.perform(post("/api/distribution-months")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distributionMonth)))
            .andExpect(status().isCreated());

        // Validate the DistributionMonth in the database
        List<DistributionMonth> distributionMonthList = distributionMonthRepository.findAll();
        assertThat(distributionMonthList).hasSize(databaseSizeBeforeCreate + 1);
        DistributionMonth testDistributionMonth = distributionMonthList.get(distributionMonthList.size() - 1);
        assertThat(testDistributionMonth.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDistributionMonth.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testDistributionMonth.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createDistributionMonthWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = distributionMonthRepository.findAll().size();

        // Create the DistributionMonth with an existing ID
        distributionMonth.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDistributionMonthMockMvc.perform(post("/api/distribution-months")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distributionMonth)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DistributionMonth> distributionMonthList = distributionMonthRepository.findAll();
        assertThat(distributionMonthList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDistributionMonths() throws Exception {
        // Initialize the database
        distributionMonthRepository.saveAndFlush(distributionMonth);

        // Get all the distributionMonthList
        restDistributionMonthMockMvc.perform(get("/api/distribution-months?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(distributionMonth.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    public void getDistributionMonth() throws Exception {
        // Initialize the database
        distributionMonthRepository.saveAndFlush(distributionMonth);

        // Get the distributionMonth
        restDistributionMonthMockMvc.perform(get("/api/distribution-months/{id}", distributionMonth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(distributionMonth.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDistributionMonth() throws Exception {
        // Get the distributionMonth
        restDistributionMonthMockMvc.perform(get("/api/distribution-months/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDistributionMonth() throws Exception {
        // Initialize the database
        distributionMonthRepository.saveAndFlush(distributionMonth);
        int databaseSizeBeforeUpdate = distributionMonthRepository.findAll().size();

        // Update the distributionMonth
        DistributionMonth updatedDistributionMonth = distributionMonthRepository.findOne(distributionMonth.getId());
        updatedDistributionMonth
            .name(UPDATED_NAME)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .status(UPDATED_STATUS);

        restDistributionMonthMockMvc.perform(put("/api/distribution-months")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDistributionMonth)))
            .andExpect(status().isOk());

        // Validate the DistributionMonth in the database
        List<DistributionMonth> distributionMonthList = distributionMonthRepository.findAll();
        assertThat(distributionMonthList).hasSize(databaseSizeBeforeUpdate);
        DistributionMonth testDistributionMonth = distributionMonthList.get(distributionMonthList.size() - 1);
        assertThat(testDistributionMonth.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDistributionMonth.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testDistributionMonth.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingDistributionMonth() throws Exception {
        int databaseSizeBeforeUpdate = distributionMonthRepository.findAll().size();

        // Create the DistributionMonth

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDistributionMonthMockMvc.perform(put("/api/distribution-months")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distributionMonth)))
            .andExpect(status().isCreated());

        // Validate the DistributionMonth in the database
        List<DistributionMonth> distributionMonthList = distributionMonthRepository.findAll();
        assertThat(distributionMonthList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDistributionMonth() throws Exception {
        // Initialize the database
        distributionMonthRepository.saveAndFlush(distributionMonth);
        int databaseSizeBeforeDelete = distributionMonthRepository.findAll().size();

        // Get the distributionMonth
        restDistributionMonthMockMvc.perform(delete("/api/distribution-months/{id}", distributionMonth.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DistributionMonth> distributionMonthList = distributionMonthRepository.findAll();
        assertThat(distributionMonthList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DistributionMonth.class);
        DistributionMonth distributionMonth1 = new DistributionMonth();
        distributionMonth1.setId(1L);
        DistributionMonth distributionMonth2 = new DistributionMonth();
        distributionMonth2.setId(distributionMonth1.getId());
        assertThat(distributionMonth1).isEqualTo(distributionMonth2);
        distributionMonth2.setId(2L);
        assertThat(distributionMonth1).isNotEqualTo(distributionMonth2);
        distributionMonth1.setId(null);
        assertThat(distributionMonth1).isNotEqualTo(distributionMonth2);
    }
}
