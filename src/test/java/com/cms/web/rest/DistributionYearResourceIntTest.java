package com.cms.web.rest;

import com.cms.CmsApp;

import com.cms.domain.DistributionYear;
import com.cms.repository.DistributionYearRepository;
import com.cms.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DistributionYearResource REST controller.
 *
 * @see DistributionYearResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CmsApp.class)
public class DistributionYearResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_LAST_UPDATE = 1L;
    private static final Long UPDATED_LAST_UPDATE = 2L;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private DistributionYearRepository distributionYearRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDistributionYearMockMvc;

    private DistributionYear distributionYear;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DistributionYearResource distributionYearResource = new DistributionYearResource(distributionYearRepository);
        this.restDistributionYearMockMvc = MockMvcBuilders.standaloneSetup(distributionYearResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DistributionYear createEntity(EntityManager em) {
        DistributionYear distributionYear = new DistributionYear()
            .name(DEFAULT_NAME)
            .lastUpdate(DEFAULT_LAST_UPDATE)
            .status(DEFAULT_STATUS);
        return distributionYear;
    }

    @Before
    public void initTest() {
        distributionYear = createEntity(em);
    }

    @Test
    @Transactional
    public void createDistributionYear() throws Exception {
        int databaseSizeBeforeCreate = distributionYearRepository.findAll().size();

        // Create the DistributionYear
        restDistributionYearMockMvc.perform(post("/api/distribution-years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distributionYear)))
            .andExpect(status().isCreated());

        // Validate the DistributionYear in the database
        List<DistributionYear> distributionYearList = distributionYearRepository.findAll();
        assertThat(distributionYearList).hasSize(databaseSizeBeforeCreate + 1);
        DistributionYear testDistributionYear = distributionYearList.get(distributionYearList.size() - 1);
        assertThat(testDistributionYear.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDistributionYear.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testDistributionYear.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createDistributionYearWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = distributionYearRepository.findAll().size();

        // Create the DistributionYear with an existing ID
        distributionYear.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDistributionYearMockMvc.perform(post("/api/distribution-years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distributionYear)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DistributionYear> distributionYearList = distributionYearRepository.findAll();
        assertThat(distributionYearList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDistributionYears() throws Exception {
        // Initialize the database
        distributionYearRepository.saveAndFlush(distributionYear);

        // Get all the distributionYearList
        restDistributionYearMockMvc.perform(get("/api/distribution-years?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(distributionYear.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    public void getDistributionYear() throws Exception {
        // Initialize the database
        distributionYearRepository.saveAndFlush(distributionYear);

        // Get the distributionYear
        restDistributionYearMockMvc.perform(get("/api/distribution-years/{id}", distributionYear.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(distributionYear.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDistributionYear() throws Exception {
        // Get the distributionYear
        restDistributionYearMockMvc.perform(get("/api/distribution-years/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDistributionYear() throws Exception {
        // Initialize the database
        distributionYearRepository.saveAndFlush(distributionYear);
        int databaseSizeBeforeUpdate = distributionYearRepository.findAll().size();

        // Update the distributionYear
        DistributionYear updatedDistributionYear = distributionYearRepository.findOne(distributionYear.getId());
        updatedDistributionYear
            .name(UPDATED_NAME)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .status(UPDATED_STATUS);

        restDistributionYearMockMvc.perform(put("/api/distribution-years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDistributionYear)))
            .andExpect(status().isOk());

        // Validate the DistributionYear in the database
        List<DistributionYear> distributionYearList = distributionYearRepository.findAll();
        assertThat(distributionYearList).hasSize(databaseSizeBeforeUpdate);
        DistributionYear testDistributionYear = distributionYearList.get(distributionYearList.size() - 1);
        assertThat(testDistributionYear.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDistributionYear.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testDistributionYear.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingDistributionYear() throws Exception {
        int databaseSizeBeforeUpdate = distributionYearRepository.findAll().size();

        // Create the DistributionYear

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDistributionYearMockMvc.perform(put("/api/distribution-years")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distributionYear)))
            .andExpect(status().isCreated());

        // Validate the DistributionYear in the database
        List<DistributionYear> distributionYearList = distributionYearRepository.findAll();
        assertThat(distributionYearList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDistributionYear() throws Exception {
        // Initialize the database
        distributionYearRepository.saveAndFlush(distributionYear);
        int databaseSizeBeforeDelete = distributionYearRepository.findAll().size();

        // Get the distributionYear
        restDistributionYearMockMvc.perform(delete("/api/distribution-years/{id}", distributionYear.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DistributionYear> distributionYearList = distributionYearRepository.findAll();
        assertThat(distributionYearList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DistributionYear.class);
        DistributionYear distributionYear1 = new DistributionYear();
        distributionYear1.setId(1L);
        DistributionYear distributionYear2 = new DistributionYear();
        distributionYear2.setId(distributionYear1.getId());
        assertThat(distributionYear1).isEqualTo(distributionYear2);
        distributionYear2.setId(2L);
        assertThat(distributionYear1).isNotEqualTo(distributionYear2);
        distributionYear1.setId(null);
        assertThat(distributionYear1).isNotEqualTo(distributionYear2);
    }
}
