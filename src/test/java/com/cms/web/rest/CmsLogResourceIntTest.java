package com.cms.web.rest;

import com.cms.CmsApp;

import com.cms.domain.CmsLog;
import com.cms.repository.CmsLogRepository;
import com.cms.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CmsLogResource REST controller.
 *
 * @see CmsLogResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CmsApp.class)
public class CmsLogResourceIntTest {

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final Long DEFAULT_LAST_UPDATE = 1L;
    private static final Long UPDATED_LAST_UPDATE = 2L;

    @Autowired
    private CmsLogRepository cmsLogRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCmsLogMockMvc;

    private CmsLog cmsLog;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CmsLogResource cmsLogResource = new CmsLogResource(cmsLogRepository);
        this.restCmsLogMockMvc = MockMvcBuilders.standaloneSetup(cmsLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CmsLog createEntity(EntityManager em) {
        CmsLog cmsLog = new CmsLog()
            .action(DEFAULT_ACTION)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return cmsLog;
    }

    @Before
    public void initTest() {
        cmsLog = createEntity(em);
    }

    @Test
    @Transactional
    public void createCmsLog() throws Exception {
        int databaseSizeBeforeCreate = cmsLogRepository.findAll().size();

        // Create the CmsLog
        restCmsLogMockMvc.perform(post("/api/cms-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cmsLog)))
            .andExpect(status().isCreated());

        // Validate the CmsLog in the database
        List<CmsLog> cmsLogList = cmsLogRepository.findAll();
        assertThat(cmsLogList).hasSize(databaseSizeBeforeCreate + 1);
        CmsLog testCmsLog = cmsLogList.get(cmsLogList.size() - 1);
        assertThat(testCmsLog.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testCmsLog.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createCmsLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cmsLogRepository.findAll().size();

        // Create the CmsLog with an existing ID
        cmsLog.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCmsLogMockMvc.perform(post("/api/cms-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cmsLog)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CmsLog> cmsLogList = cmsLogRepository.findAll();
        assertThat(cmsLogList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCmsLogs() throws Exception {
        // Initialize the database
        cmsLogRepository.saveAndFlush(cmsLog);

        // Get all the cmsLogList
        restCmsLogMockMvc.perform(get("/api/cms-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cmsLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.intValue())));
    }

    @Test
    @Transactional
    public void getCmsLog() throws Exception {
        // Initialize the database
        cmsLogRepository.saveAndFlush(cmsLog);

        // Get the cmsLog
        restCmsLogMockMvc.perform(get("/api/cms-logs/{id}", cmsLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cmsLog.getId().intValue()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCmsLog() throws Exception {
        // Get the cmsLog
        restCmsLogMockMvc.perform(get("/api/cms-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCmsLog() throws Exception {
        // Initialize the database
        cmsLogRepository.saveAndFlush(cmsLog);
        int databaseSizeBeforeUpdate = cmsLogRepository.findAll().size();

        // Update the cmsLog
        CmsLog updatedCmsLog = cmsLogRepository.findOne(cmsLog.getId());
        updatedCmsLog
            .action(UPDATED_ACTION)
            .lastUpdate(UPDATED_LAST_UPDATE);

        restCmsLogMockMvc.perform(put("/api/cms-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCmsLog)))
            .andExpect(status().isOk());

        // Validate the CmsLog in the database
        List<CmsLog> cmsLogList = cmsLogRepository.findAll();
        assertThat(cmsLogList).hasSize(databaseSizeBeforeUpdate);
        CmsLog testCmsLog = cmsLogList.get(cmsLogList.size() - 1);
        assertThat(testCmsLog.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testCmsLog.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingCmsLog() throws Exception {
        int databaseSizeBeforeUpdate = cmsLogRepository.findAll().size();

        // Create the CmsLog

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCmsLogMockMvc.perform(put("/api/cms-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cmsLog)))
            .andExpect(status().isCreated());

        // Validate the CmsLog in the database
        List<CmsLog> cmsLogList = cmsLogRepository.findAll();
        assertThat(cmsLogList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCmsLog() throws Exception {
        // Initialize the database
        cmsLogRepository.saveAndFlush(cmsLog);
        int databaseSizeBeforeDelete = cmsLogRepository.findAll().size();

        // Get the cmsLog
        restCmsLogMockMvc.perform(delete("/api/cms-logs/{id}", cmsLog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CmsLog> cmsLogList = cmsLogRepository.findAll();
        assertThat(cmsLogList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CmsLog.class);
        CmsLog cmsLog1 = new CmsLog();
        cmsLog1.setId(1L);
        CmsLog cmsLog2 = new CmsLog();
        cmsLog2.setId(cmsLog1.getId());
        assertThat(cmsLog1).isEqualTo(cmsLog2);
        cmsLog2.setId(2L);
        assertThat(cmsLog1).isNotEqualTo(cmsLog2);
        cmsLog1.setId(null);
        assertThat(cmsLog1).isNotEqualTo(cmsLog2);
    }
}
