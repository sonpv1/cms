package com.cms.web.rest;

import com.cms.CmsApp;

import com.cms.domain.TargetMetric;
import com.cms.repository.TargetMetricRepository;
import com.cms.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TargetMetricResource REST controller.
 *
 * @see TargetMetricResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CmsApp.class)
public class TargetMetricResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final Integer DEFAULT_METRIC = 1;
    private static final Integer UPDATED_METRIC = 2;

    private static final Integer DEFAULT_COST = 1;
    private static final Integer UPDATED_COST = 2;

    private static final String DEFAULT_COST_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_COST_TYPE = "BBBBBBBBBB";

    private static final Long DEFAULT_LAST_UPDATE = 1L;
    private static final Long UPDATED_LAST_UPDATE = 2L;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private TargetMetricRepository targetMetricRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTargetMetricMockMvc;

    private TargetMetric targetMetric;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TargetMetricResource targetMetricResource = new TargetMetricResource(targetMetricRepository);
        this.restTargetMetricMockMvc = MockMvcBuilders.standaloneSetup(targetMetricResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TargetMetric createEntity(EntityManager em) {
        TargetMetric targetMetric = new TargetMetric()
            .name(DEFAULT_NAME)
            .department(DEFAULT_DEPARTMENT)
            .metric(DEFAULT_METRIC)
            .cost(DEFAULT_COST)
            .costType(DEFAULT_COST_TYPE)
            .lastUpdate(DEFAULT_LAST_UPDATE)
            .status(DEFAULT_STATUS);
        return targetMetric;
    }

    @Before
    public void initTest() {
        targetMetric = createEntity(em);
    }

    @Test
    @Transactional
    public void createTargetMetric() throws Exception {
        int databaseSizeBeforeCreate = targetMetricRepository.findAll().size();

        // Create the TargetMetric
        restTargetMetricMockMvc.perform(post("/api/target-metrics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(targetMetric)))
            .andExpect(status().isCreated());

        // Validate the TargetMetric in the database
        List<TargetMetric> targetMetricList = targetMetricRepository.findAll();
        assertThat(targetMetricList).hasSize(databaseSizeBeforeCreate + 1);
        TargetMetric testTargetMetric = targetMetricList.get(targetMetricList.size() - 1);
        assertThat(testTargetMetric.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTargetMetric.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testTargetMetric.getMetric()).isEqualTo(DEFAULT_METRIC);
        assertThat(testTargetMetric.getCost()).isEqualTo(DEFAULT_COST);
        assertThat(testTargetMetric.getCostType()).isEqualTo(DEFAULT_COST_TYPE);
        assertThat(testTargetMetric.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testTargetMetric.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTargetMetricWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = targetMetricRepository.findAll().size();

        // Create the TargetMetric with an existing ID
        targetMetric.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTargetMetricMockMvc.perform(post("/api/target-metrics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(targetMetric)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TargetMetric> targetMetricList = targetMetricRepository.findAll();
        assertThat(targetMetricList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTargetMetrics() throws Exception {
        // Initialize the database
        targetMetricRepository.saveAndFlush(targetMetric);

        // Get all the targetMetricList
        restTargetMetricMockMvc.perform(get("/api/target-metrics?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(targetMetric.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT.toString())))
            .andExpect(jsonPath("$.[*].metric").value(hasItem(DEFAULT_METRIC)))
            .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST)))
            .andExpect(jsonPath("$.[*].costType").value(hasItem(DEFAULT_COST_TYPE.toString())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    public void getTargetMetric() throws Exception {
        // Initialize the database
        targetMetricRepository.saveAndFlush(targetMetric);

        // Get the targetMetric
        restTargetMetricMockMvc.perform(get("/api/target-metrics/{id}", targetMetric.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(targetMetric.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT.toString()))
            .andExpect(jsonPath("$.metric").value(DEFAULT_METRIC))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST))
            .andExpect(jsonPath("$.costType").value(DEFAULT_COST_TYPE.toString()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTargetMetric() throws Exception {
        // Get the targetMetric
        restTargetMetricMockMvc.perform(get("/api/target-metrics/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTargetMetric() throws Exception {
        // Initialize the database
        targetMetricRepository.saveAndFlush(targetMetric);
        int databaseSizeBeforeUpdate = targetMetricRepository.findAll().size();

        // Update the targetMetric
        TargetMetric updatedTargetMetric = targetMetricRepository.findOne(targetMetric.getId());
        updatedTargetMetric
            .name(UPDATED_NAME)
            .department(UPDATED_DEPARTMENT)
            .metric(UPDATED_METRIC)
            .cost(UPDATED_COST)
            .costType(UPDATED_COST_TYPE)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .status(UPDATED_STATUS);

        restTargetMetricMockMvc.perform(put("/api/target-metrics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTargetMetric)))
            .andExpect(status().isOk());

        // Validate the TargetMetric in the database
        List<TargetMetric> targetMetricList = targetMetricRepository.findAll();
        assertThat(targetMetricList).hasSize(databaseSizeBeforeUpdate);
        TargetMetric testTargetMetric = targetMetricList.get(targetMetricList.size() - 1);
        assertThat(testTargetMetric.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTargetMetric.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testTargetMetric.getMetric()).isEqualTo(UPDATED_METRIC);
        assertThat(testTargetMetric.getCost()).isEqualTo(UPDATED_COST);
        assertThat(testTargetMetric.getCostType()).isEqualTo(UPDATED_COST_TYPE);
        assertThat(testTargetMetric.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testTargetMetric.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingTargetMetric() throws Exception {
        int databaseSizeBeforeUpdate = targetMetricRepository.findAll().size();

        // Create the TargetMetric

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTargetMetricMockMvc.perform(put("/api/target-metrics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(targetMetric)))
            .andExpect(status().isCreated());

        // Validate the TargetMetric in the database
        List<TargetMetric> targetMetricList = targetMetricRepository.findAll();
        assertThat(targetMetricList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTargetMetric() throws Exception {
        // Initialize the database
        targetMetricRepository.saveAndFlush(targetMetric);
        int databaseSizeBeforeDelete = targetMetricRepository.findAll().size();

        // Get the targetMetric
        restTargetMetricMockMvc.perform(delete("/api/target-metrics/{id}", targetMetric.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TargetMetric> targetMetricList = targetMetricRepository.findAll();
        assertThat(targetMetricList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TargetMetric.class);
        TargetMetric targetMetric1 = new TargetMetric();
        targetMetric1.setId(1L);
        TargetMetric targetMetric2 = new TargetMetric();
        targetMetric2.setId(targetMetric1.getId());
        assertThat(targetMetric1).isEqualTo(targetMetric2);
        targetMetric2.setId(2L);
        assertThat(targetMetric1).isNotEqualTo(targetMetric2);
        targetMetric1.setId(null);
        assertThat(targetMetric1).isNotEqualTo(targetMetric2);
    }
}
